//
//  PhysicsCategorie.swift
//  ouidou-meetup
//
//  Created by Jordan on 22/07/2019.
//  Copyright © 2019 Jordan. All rights reserved.
//

import Foundation

struct PhysicsCategory {
    
    static let None                 : UInt32 = 0                // Masque 0
    
    static let Player               : UInt32 = 0b01             // Masque 1
    
    static let Enemy                : UInt32 = 0b10             // Masque 2
    
    static let Ground               : UInt32 = 0b11             // Masque 3
}
