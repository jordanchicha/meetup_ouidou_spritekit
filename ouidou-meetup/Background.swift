//
//  Background.swift
//  ouidou-meetup
//
//  Created by Jordan on 04/07/2019.
//  Copyright © 2019 Jordan. All rights reserved.
//

import Foundation
import SpriteKit

class Background {
    
    var tupleBackground = [(SKSpriteNode, SKSpriteNode)]()
    
    init(imageNamed : String, size: CGSize) {
        
        for i in 1...5 {
            tupleBackground.append(((SKSpriteNode(imageNamed: "\(imageNamed)_\(i)")), (SKSpriteNode(imageNamed: "\(imageNamed)_\(i)"))))
        }
        
        var i = 1
        
        for tuple in tupleBackground {
            tuple.0.zPosition = CGFloat(i)
            tuple.1.zPosition = CGFloat(i)
            
            tuple.0.size = size
            tuple.1.size = size
            
            tuple.0.anchorPoint = CGPoint.zero
            tuple.1.anchorPoint = CGPoint.zero
            
            tuple.0.position.y = 0
            tuple.1.position.x = tuple.1.size.width - CGFloat(i)
            
            if i == 5 {
                tuple.0.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: tuple.0.size.width, height: 62))
                tuple.1.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: tuple.1.size.width, height: 62))
                tuple.0.physicsBody?.isDynamic = false
                tuple.1.physicsBody?.isDynamic = false
                tuple.0.physicsBody?.categoryBitMask = PhysicsCategory.Ground
                tuple.1.physicsBody?.categoryBitMask = PhysicsCategory.Ground
            }
            
            i += 1
        }
        
    }
    
    
    func scroll() {
        
        var speed = 1
        
        for tuple in self.tupleBackground {
            if tuple.0.position.x < -tuple.0.size.width
            {
                tuple.0.position.x = tuple.1.position.x + tuple.1.size.width
            }
            if tuple.1.position.x < -tuple.1.size.width
            {
                tuple.1.position.x = tuple.0.position.x + tuple.0.size.width
            }
            tuple.0.position.x -= CGFloat(speed)
            tuple.1.position.x -= CGFloat(speed)
            
            speed += 1
        }
    }
    
}
