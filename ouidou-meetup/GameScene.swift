//
//  GameScene.swift
//  ouidou-meetup
//
//  Created by Jordan on 04/07/2019.
//  Copyright © 2019 Jordan. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene, SKPhysicsContactDelegate {
    
    var background: Background!
    
    var player: Player!
    
    var lives: [SKSpriteNode]!
    
    var transition: SKAction!
    
    var labelScore: SKLabelNode!
    
    var score: Int!
    
    override func didMove(to view: SKView) {
        
        // Score
        score = 0
        
        
        // Gravite
        physicsWorld.contactDelegate = self
        self.physicsWorld.gravity = CGVector(dx: 0.0, dy: -14)
    
        
        // Action de transition vers Game Over
        transition = SKAction.run() { [weak self] in
            guard let `self` = self else { return }
            let reveal = SKTransition.flipHorizontal(withDuration: 0.5)
            let gameOverScene = GameOverScene(size: self.size)
            self.view?.presentScene(gameOverScene, transition: reveal)
        }
        
        
        // Fond d'ecran
        background = Background(imageNamed: "desert", size: self.size)
        
        for tuple in background.tupleBackground {
            addChild(tuple.0)
            addChild(tuple.1)
        }
        
        
        // Joueur
        player = Player(imageNamed: "run_1",
                        lives: 3,
                        sizeCoefficient: 0.18,
                        position: CGPoint(x: 120, y: 62),
                        scene: self)
        
        
        // Icones de vie
        lives = [SKSpriteNode]()
        
        for i in 0...2 {
            lives.append(SKSpriteNode(imageNamed: "vie"))
            lives[i].size = CGSize(width: lives[i].size.width * 0.35,
                                   height: lives[i].size.height * 0.35)
            lives[i].position = CGPoint(x: self.size.width - (CGFloat(i) + 1.5) * lives[i].size.width,
                                        y: self.size.height - 1.4 * lives[i].size.height)
            lives[i].zPosition = 10
            addChild(lives[i])
        }
        
        // Label du score
        labelScore = SKLabelNode(fontNamed: "AvenirNext-Bold")
        labelScore.text = "\(score ?? 0)"
        labelScore.fontSize = 34
        labelScore.fontColor = UIColor.white
        labelScore.position = CGPoint(x: 1.5 * lives[0].size.width, y: self.size.height - 1.4 * lives[0].size.width)
        labelScore.zPosition = 10
        
        
        // Action d'apparition des ennemis
        run(
            SKAction.repeatForever(
                SKAction.sequence([
                    SKAction.run(addBird),
                    SKAction.wait(forDuration: 1.0)
                ])
            )
        )
        
        // Ajout des elements dans la scene
        addChild(labelScore)
        addChild(player)
        
        // Le joueur court
        player.run()
    }
    
    override func update(_ currentTime: TimeInterval) {
        labelScore.text = "\(score ?? 0)"
        background.scroll()
        player.position.x = 120
        player.zRotation = CGFloat((Double.pi * 2))
    }
        
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if player.action(forKey: "jump") == nil && player.position.y > 63 {
            player.jump()
            player.physicsBody?.applyImpulse(CGVector(dx: 0, dy: 60))
        }
    }
    
    func didBegin(_ contact: SKPhysicsContact) {
        
        var firstBody: SKPhysicsBody
        var secondBody: SKPhysicsBody
        
        if contact.bodyA.categoryBitMask < contact.bodyB.categoryBitMask {
            firstBody = contact.bodyA
            secondBody = contact.bodyB
        } else {
            firstBody = contact.bodyB
            secondBody = contact.bodyA
        }
        
        if ((firstBody.categoryBitMask == PhysicsCategory.Enemy) &&
            (secondBody.categoryBitMask == PhysicsCategory.Player) ||
            (firstBody.categoryBitMask == PhysicsCategory.Player) &&
            (secondBody.categoryBitMask == PhysicsCategory.Enemy)) {
            if firstBody.node?.name == "player" {
                secondBody.node?.removeFromParent()
            }
            else {
                firstBody.node?.removeFromParent()
            }
            player.minusOneLife()
            if player.getLives() == 0 {
                self.run(self.transition)
            }
            else {
                lives[player.getLives()].removeFromParent()
            }
            
        }
    }
    
    func addBird() {
        
        let randomBird = randomInt(min: 1, max: 4)
        
        let randomPoint = CGPoint(x: size.width, y: random(min: 60, max: 120))
        
        var birds = [1: "red", 2: "grey", 3: "yellow", 4: "blue"]
        
        let enemy = Enemy(imageNamed: "\(birds[randomBird] ?? "red")_1", sizeCoefficient: 0.07, position: randomPoint, scene: self)
   
        addChild(enemy)
        enemy.move(imageNamed: birds[randomBird] ?? "red")
        
        score += 1
    }
    
    
}
