//
//  Utilities.swift
//  ouidou-meetup
//
//  Created by Jordan on 22/07/2019.
//  Copyright © 2019 Jordan. All rights reserved.
//

import Foundation
import SpriteKit

func random() -> CGFloat {
    return CGFloat(Float(arc4random()) / 0xFFFFFFFF)
}

func random(min: CGFloat, max: CGFloat) -> CGFloat {
    return random() * (max - min) + min
}

func randomInt(min: Int, max:Int) -> Int {
    return min + Int(arc4random_uniform(UInt32(max - min + 1)))
}
