//
//  Player.swift
//  ouidou-meetup
//
//  Created by Jordan on 11/07/2019.
//  Copyright © 2019 Jordan. All rights reserved.
//

import Foundation
import SpriteKit

class Player: SKSpriteNode {
    
    var lives: Int!
    
    override init(texture: SKTexture?, color: UIColor, size: CGSize) {
        super.init(texture: texture, color: color, size: size)
    }
    
    init(imageNamed: String, lives: Int, sizeCoefficient: CGFloat, position: CGPoint, scene: SKScene) {
        self.init(imageNamed: imageNamed)
        self.name = "player"
        self.lives = lives
        self.size = CGSize(width: self.size.width * sizeCoefficient , height: self.size.height * sizeCoefficient)
        self.position = position
        self.zPosition = 10
        
        self.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: self.size.width * 0.25, height: self.size.height * 0.8))
        self.physicsBody?.isDynamic = true
        self.physicsBody?.categoryBitMask = PhysicsCategory.Player
        self.physicsBody?.contactTestBitMask = PhysicsCategory.Enemy
        self.physicsBody?.usesPreciseCollisionDetection = true
    }
    
    func run() {
        var runFrames: [SKTexture] = []
        
        for i in 1...8 {
            runFrames.append(SKTexture(imageNamed: "run_\(i)"))
        }
        
        self.run(
            SKAction.repeatForever(
                SKAction.animate(
                    with: runFrames,
                    timePerFrame: 0.1,
                    resize: false,
                    restore: true
                )
            ), withKey: "run"
        )
    }
    
    func jump() {
        
        var jumpFrames: [SKTexture] = []

        for i in 1...12 {
            jumpFrames.append(SKTexture(imageNamed: "jump_\(i)"))
        }

        let animationJump = SKAction.animate(with: jumpFrames,
                                             timePerFrame: 0.05,
                                             resize: false,
                                             restore: false)
        
        self.run(animationJump, withKey: "jump")
    }
    
    func minusOneLife() {
        self.lives -= 1
    }
    
    func getLives() -> Int {
        return self.lives
    }
    
    // Obligatoire
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
