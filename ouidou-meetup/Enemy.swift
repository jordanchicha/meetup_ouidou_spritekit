//
//  Enemy.swift
//  ouidou-meetup
//
//  Created by Jordan on 22/07/2019.
//  Copyright © 2019 Jordan. All rights reserved.
//

import Foundation
import SpriteKit

class Enemy: SKSpriteNode {
    
    override init(texture: SKTexture?, color: UIColor, size: CGSize) {
        super.init(texture: texture, color: color, size: size)
    }
    
    init(imageNamed: String, sizeCoefficient: CGFloat, position: CGPoint, scene: SKScene) {
        
        self.init(imageNamed: imageNamed)
        self.size = CGSize(width: self.size.width * sizeCoefficient , height: self.size.height * sizeCoefficient)
        self.position = position
        self.zPosition = 10
        self.xScale = -1.0
        
        self.physicsBody = SKPhysicsBody(circleOfRadius: 5)
        self.physicsBody?.isDynamic = false
        self.physicsBody?.categoryBitMask = PhysicsCategory.Enemy
        self.physicsBody?.contactTestBitMask = PhysicsCategory.Player
        self.physicsBody?.collisionBitMask = PhysicsCategory.None
        self.physicsBody?.usesPreciseCollisionDetection = true
    }
    
    func move(imageNamed: String) {
        var runFrames: [SKTexture] = []
        
        for i in 1...4 {
            runFrames.append(SKTexture(imageNamed: "\(imageNamed)_\(i)"))
        }
        
        self.run(
            SKAction.repeatForever(
                SKAction.animate(
                    with: runFrames,
                    timePerFrame: 0.1,
                    resize: false,
                    restore: true
                )
            ), withKey: "fly"
        )
        
        let path = UIBezierPath()
        path.move(to: self.position)
        path.addLine(to: CGPoint(x: 0, y: self.position.y))
        
        let move = SKAction.follow(path.cgPath,
                                   asOffset: false,
                                   orientToPath: false,
                                   speed: CGFloat(randomInt(min: 600, max: 700)))
        
        let remove = SKAction.removeFromParent()
        
        self.run(SKAction.sequence([move, remove]))
    }
    
    // Obligatoire
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
